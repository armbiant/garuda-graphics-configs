# Maintainer: TNE <tne@garudalinux.org>

pkgbase=garuda-nvidia-config
pkgname=('garuda-virtualmachine-guest-config' 'garuda-video-linux-config')
pkgver=1.1.4
pkgrel=2
pkgdesc=""
arch=('any')
license=('GPL')
source=('90-nvidia-prime-powermanagement.rules' 'nvidia-prime-powersaving.conf' 'modules-load' 'garuda-virtualmachine-guest-config.target' 'garuda-optimus-manager-config.hook' 'optimus-manager-qt.desktop')
sha256sums=('b7f7f63a21e540142f1d1154b8cacaa9a6246fdcca265993a13a45e6bc99a60a'
            'b44093d0a19f8fc16977abb6eae2bfe9a55af68f2d8804c65cb60a65ca130844'
            '34dc136c9e7c06af2a59ff94263397a707323d0aa523f8b988ec6077448e7716'
            'a6589345fac5cccea3a7830d68c83704b32ebc0f28720431e34656cae9589480'
            '387a98be7c12a0edd95bd8122145e27e612fe098d349e43e5084a8f7e9134b55'
            'bbc25e7c01820eeac8b08e378a518ef7a3c952e3238fa45b04957d15ed2a57df')

_VERSIONLIST=("470" "latest")

_generate_settings() {
  _depends=('egl-wayland' 'gwe' "nvidia${_VERSTR}-utils" "nvidia${_VERSTR}-settings" "opencl-nvidia${_VERSTR}" "lib32-nvidia${_VERSTR}-utils" "lib32-opencl-nvidia${_VERSTR}" "nvidia${_VERSTR}-dkms")

  _depends_prime=("garuda-nvidia${_VERSTR}-config" 'nvidia-prime' 'garuda-video-linux-config')
  _depends_manager=("garuda-nvidia${_VERSTR}-config" 'nvidia-prime' 'optimus-manager' 'optimus-manager-qt' 'bbswitch-dkms' 'acpi_call-dkms' 'garuda-video-linux-config')

  _conflicts=()
  _conflicts_prime=("garuda-optimus-manager${_VERSTR}-config" 'optimus-manager')
  _conflicts_manager=("garuda-nvidia${_VERSTR}-prime-config")


  # Generate conflicts for version
  for i in "${_VERSIONLIST[@]/$1}"
  do
    [ -z "$i" ] && continue
    if [ "$i" == "latest" ]; then
      local _VERSTR_CON=""
    else
      local _VERSTR_CON="-${i}xx"
    fi
    _conflicts+=("garuda-nvidia${_VERSTR_CON}-config" "garuda-nvidia${_VERSTR_CON}-prime-config" "garuda-optimus-manager${_VERSTR_CON}-config")
  done

  pkgname+=("garuda-nvidia${_VERSTR}-config" "garuda-nvidia${_VERSTR}-prime-config" "garuda-optimus-manager${_VERSTR}-config")
}

_nvidia_function_factory() {
  if [ "$1" == "latest" ]; then
    _VERSTR=""
  else
    _VERSTR="-$1xx"
  fi

  _generate_settings $1
  local text
  printf -v text 'package_garuda-nvidia%s-config() { pkgdesc="Meta configuration package for nvidia systems on Garuda Linux"; arch=("any"); depends=(%s); conflicts=(%s); cd $srcdir; install -Dm644 modules-load $pkgdir/etc/modules-load.d/garuda-nvidia.conf; }; package_garuda-nvidia%s-prime-config() { pkgdesc="Meta configuration package for nvidia prime systems on Garuda Linux": arch=("any"); depends=(%s); conflicts=(%s); cd $srcdir; install -Dm644 nvidia-prime-powersaving.conf $pkgdir/etc/modprobe.d/nvidia-prime-powersaving.conf; install -Dm644 90-nvidia-prime-powermanagement.rules $pkgdir/etc/udev/rules.d/90-nvidia-prime-powermanagement.rules; }; package_garuda-optimus-manager%s-config() { pkgdesc="Meta configuration package for nvidia prime systems on Garuda Linux": arch=("any"); depends=(%s); conflicts=(%s); cd $srcdir; install -Dm644 90-nvidia-prime-powermanagement.rules $pkgdir/etc/udev/rules.d/90-nvidia-prime-powermanagement.rules; install -Dm644 garuda-optimus-manager-config.hook $pkgdir/usr/share/libalpm/hooks/garuda-optimus-manager-config.hook; install -Dm644 optimus-manager-qt.desktop $pkgdir/etc/xdg/autostart/optimus-manager-qt.desktop; }' "$_VERSTR" "${_depends[*]}" "${_conflicts[*]}" "$_VERSTR" "${_depends_prime[*]}" "${_conflicts_prime[*]}" "$_VERSTR" "${_depends_manager[*]}" "${_conflicts_manager[*]}"
  eval "$text"
}

package_garuda-virtualmachine-guest-config() {
  pkgdesc="Meta configuration package for virtual machine systems on Garuda Linux"
  arch=('any')
  depends=('virtualbox-guest-utils' 'xf86-video-vmware' 'open-vm-tools' 'xf86-input-vmmouse' 'spice-vdagent' 'qemu-guest-agent' 'gtkmm3')
  install="garuda-virtualmachine-guest-config.install"

  cd $srcdir
  install -Dm644 garuda-virtualmachine-guest-config.target $pkgdir/usr/lib/systemd/system/garuda-virtualmachine-guest-config.target
}

package_garuda-video-linux-config() {
  pkgdesc="Meta configuration package for common open source drivers on Garuda Linux"
  arch=('any')
  depends=(xf86-video-ati xf86-video-amdgpu xf86-video-intel xf86-video-nouveau vulkan-intel vulkan-radeon opencl-mesa intel-compute-runtime intel-media-driver libvdpau-va-gl libva-intel-driver libva-mesa-driver libva-vdpau-driver mesa-vdpau vulkan-mesa-layers vulkan-swrast lib32-vulkan-intel lib32-vulkan-radeon lib32-opencl-mesa lib32-mesa-vdpau lib32-libva-intel-driver lib32-libva-mesa-driver lib32-libva-vdpau-driver lib32-vulkan-mesa-layers)
}

_nvidia_function_factory latest
_nvidia_function_factory 470
